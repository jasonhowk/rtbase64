//
//  RTBase64Tests.h
//  RTBase64Tests
//
//  Created by Jason Howk on 12/19/11.
//  Copyright (c) 2011 Rude Tie, LLC. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface RTBase64Tests : SenTestCase
{
    NSString *input;
    NSString *output;
    NSString *rfcInput;
    NSString *rfcOutput;
}

@end
